<?php

namespace Phpwork\Acl\Controller;

use Illuminate\Http\Request;
use Phpwork\Acl\Models\Permission;
use Phpwork\Acl\Models\Roles;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
class PermissionController extends Controller
{
    
    protected $rules = [
            'permission_title' => 'required',
        ];

   
    public function index()
    {
        $permissions = Permission::all();
        
        return view('acl::permissions.index',  compact('permissions'));
    }
    
    public function create(Permission $permissions)
    {
      $roles = Roles::lists('role_slug','id')->all();
      
      return view('acl::permissions.form', compact('permissions','roles'));
    }
    
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        
        $permission = Permission::create($request->only('permission_title','permission_slug','permission_description'));
        
        $permission->syncRoles($request->get('roles_id', []));

        return redirect(route('permissions.index'))->with('status', 'Permission has been created.');
    }

    
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        
        $permissions = Permission::findorFail($id);
        
        $roles = Roles::lists('role_slug','id')->all();       
        
        return view('acl::permissions.form', compact('permissions','roles'));
    }

    public function update(Request $request, $id)
    {
        $permission = Permission::find($id);

        $permission->fill($request->only('permission_title','permission_slug','permission_description'))->save();
        
        $permission->syncRoles($request->get('roles_id', []));

        return redirect(route('permissions.index'))->with('status', 'Permission has been created.');
    }
   
    public function destroy($id)
    {
        $permission = Permission::find($id);
        
        $permission->roles()->detach();
        
        $permission->delete();

        return redirect(route('permissions.index'))->with('status', 'Permission has been deleted.');
    }
}
