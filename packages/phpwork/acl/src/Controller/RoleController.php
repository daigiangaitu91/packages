<?php

namespace Phpwork\Acl\Controller;

use Illuminate\Http\Request;
use Phpwork\Acl\Models\Roles;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    
    public function index()
    {
        $roles = Roles::all();
        return view('acl::roles.index')->withRoles($roles);
    }
    protected $rules = [
        'role_title' => 'required|max:255|unique:roles',
        'role_slug' => 'required|max:255|unique:roles',
    ];
    
    public function create(Roles $roles)
    {
        return view('acl::roles.form', compact('roles'));
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        $roles = new Roles();
        $roles->create($request->only('role_title', 'role_slug'));

        return redirect(route('roles.index'))->with('status', 'Roles has been created.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $roles = Roles::findOrFail($id);
        return view('acl::roles.form',compact('roles'));
    }

    public function update(Request $request, $id)
    {
        $role = Roles::findOrFail($id);
        
        $role->fill($request->only('role_title', 'role_slug'))->save();

        return redirect(route('roles.index'))->with('status', 'Roles has been updated.');
    }

    
    public function confirm($id)
    {       
        $role = Roles::findOrFail($id);

        return view('acl::roles.confirm', compact('role'));
    }
    
    public function destroy($id)
    {
        $role = Roles::find($id);
        
        $role->delete();
        
        return redirect(route('roles.index'))->with('status', 'Roles has been deleted.');
    }
}
