<?php

namespace Phpwork\Acl\Controller;

use Phpwork\Acl\Models\Roles;
use Illuminate\Http\Request;
use Phpwork\Acl\Models\User;
use Phpwork\Acl\Models\Profile;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Input;
use Laravel\Socialite\Facades\Socialite;
use Redirect;

class UserController extends Controller {

  protected $rules = [
    'name' => 'required|max:255',
    'email' => 'required|email|max:255|unique:users',
    'password' => 'required|confirmed|min:6',
  ];

  public function index() {
    //$users = User::with('profile')->get();
    $users = User::all();
    return view('acl::users.index', compact('users'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create(User $user) {
    $profile = new Profile();
    return view('acl::users.form', compact('user'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {

    $this->validate($request, $this->rules);
    
    $user = User::create($request->only('name', 'email', 'password'));
    
    $profile = new Profile();
    $profile->fill($request->only('ho', 'ten', 'birthday', 'phone_number'));
    $profile->active_code = str_random(60);
//        $user->roles()->sync($request->get('roles'), []);
    $profile->is_active = 1;   
    $user->profile()->save($profile);
    return redirect(route('users.index'))->with('status', 'User has been created.');
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id) {
    $user = User::findorFail($id);
    return view('acl::users.form', compact('user'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id) {
    $this->validate($request, [
      'password' => 'required|confirmed|min:6',
    ]);
    $user = User::find($id);
    $user->profile->get();
    // Save data for table user.
    foreach (array_keys($user->fields_user()) as $field) {
      if ($user->$field != $request->get($field)) {
        $user->$field = $request->get($field);
      }
    }
    // Save password.
    $user->password = bcrypt($user->password);
    // Save data for table profile.
    foreach (array_keys($user->fields_profile()) as $field) {
      $user->profile->$field = $request->get($field);
    }
    $user->save();
    $user->roles()->sync($request->get('roles'), []);
    $user->profile->save();

    return redirect()
            ->route('admin.user.index')
            ->withSuccess("Hi '$user->name' please create profile.");
  }

  public function confirm($id) {
    $user = User::findOrFail($id);

    return view('acl::users.confirm', compact('user'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id) {
    $user = User::find($id);
//        $user->with('profile')->get();
//        $user->profile->delete();
    //$user->roles()->detach();
    $user->delete();
    return redirect(route('users.index'))->with('status', 'User has been created.');
  }

}
