
<!DOCTYPE html>
<html>
  <head>
    <title>Laravel Timezones</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >
  </head>
  <body>
    <div class="container">
      <div class="content">
        <a href="{{ route('permissions.create') }}" class="btn btn-primary">Create New Permission</a>

        <table class="table table-hover">
          <thead>
            <tr>
              <th>Permission Title</th>
              <th>Pemission Slug</th>
              <th>Pemission Description</th>
              <th>Pemission Roles</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            @foreach($permissions as $permission)
            <tr>
              <td>
                <a href="{{ route('permissions.edit', $permission->id) }}">{{ $permission->permission_title }}</a>
              </td>
              <td>{{ $permission->permission_slug }}</td>
              <td>{{ $permission->permission_description }}</td>
              <td>
                @foreach($permission['roles'] as $role)
                {{ $role['role_slug'] }} <br/>
                @endforeach
              </td>
              <td>
                <a href="{{ route('permissions.edit', $permission->id) }}">
                  <span class="glyphicon glyphicon-edit"></span>
                </a>
              </td>

              <td>
                <a href="{{ route('permissions.confirm', $permission->id) }}">
                  <span class="glyphicon glyphicon-remove"></span>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </body>
</html>