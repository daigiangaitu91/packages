
<!DOCTYPE html>
<html>
  <head>
    <title>Laravel Timezones</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >    
  </head>
  <body>
    <div class="container">      
      <div class="content">
        {!! Form::open(['method' => 'delete', 'route' => ['permissions.destroy', $permission->id]]) !!}
        <div class="alert alert-danger">
            <strong>Warning!</strong> You are about to delete a user. This action cannot be undone. Are you sure you want to continue?
        </div>

        {!! Form::submit('Yes, delete this user!', ['class' => 'btn btn-danger']) !!}

        <a href="{{ route('permissions.index') }}" class="btn btn-success">
            <strong>No, get me out of here!</strong>
        </a>
    {!! Form::close() !!}
      </div>
    </div>
  </body>
</html>