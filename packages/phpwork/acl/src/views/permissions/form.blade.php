<!DOCTYPE html>
<html>
  <head>
    <title>Laravel Timezones</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >
  </head>
  <body>
    <div class="container">
      <div class="content">
        {!! Form::model($permissions, [
        'method' => $permissions->exists ? 'put' : 'post',
        'route' => $permissions->exists ? ['permissions.update', $permissions->id] : ['permissions.store']
        ]) !!}

        <div class="form-group">
          {!! Form::label('Title') !!}
          {!! Form::text('permission_title', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
          {!! Form::label('slug') !!}
          {!! Form::text('permission_slug', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('Description') !!}
          {!! Form::text('permission_description', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group row">
          <div class="col-md-12">
            {!! Form::label('Roles') !!}
          </div>
          <div class="col-md-4">            
            @if(!$permissions->exists)
            {!! Form::select('roles_id', $roles, null, ['class' => 'form-control','multiple'=>'multiple','name'=>'roles_id[]']) !!}
            @else            
            <select multiple="multiple" name="roles_id[]" id="sports" class="form-control">
              @foreach($roles as $key => $role_slug)
                {{--*/ $selected = '' /*--}}
                @foreach($permissions['roles'] as $role)                  
                  @if($role_slug == $role['role_slug'])
                  {{--*/ $selected = 'selected="selected"' /*--}}
                  @endif                          
                @endforeach
              <option value="{{ $key }}" {{ $selected }}>
                {{$role_slug}}
              </option>
              @endforeach

            </select>
            @endif
          </div>
        </div>

        {!! Form::submit($permissions->exists ? 'Save Permission' : 'Create New Permission', ['class' => 'btn btn-primary']) !!}

        {!! Form::close() !!}
      </div>
    </div>
  </body>
</html>