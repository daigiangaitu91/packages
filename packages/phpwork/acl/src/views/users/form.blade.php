
<!DOCTYPE html>
<html>
  <head>
    <title>Laravel Timezones</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >    
  </head>
  <body>
    <div class="container">      
      <div class="content">
        {!! Form::model($user, [
        'method' => $user->exists ? 'put' : 'post',
        'route' => $user->exists ? ['users.update', $user->id] : ['users.store']
        ]) !!}

        <div class="form-group">
          {!! Form::label('name') !!}
          {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
          {!! Form::label('email') !!}
          {!! Form::text('email', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
          {!! Form::label('password') !!}
          {!! Form::password('password', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
          {!! Form::label('password_confirmation') !!}
          {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
        </div>
        
        <div class="form-group">
          {!! Form::label('Ho') !!}
          {!! Form::text('ho', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
          {!! Form::label('Ten') !!}
          {!! Form::text('ten', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
          {!! Form::label('Phone number') !!}
          {!! Form::text('phone_number', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
          {!! Form::label('Birthday') !!}
          {!! Form::text('birthday', null, ['class' => 'form-control']) !!}
        </div>
        
        {!! Form::submit($user->exists ? 'Save User' : 'Create New User', ['class' => 'btn btn-primary']) !!}

        {!! Form::close() !!}
      </div>
    
    </div>
  </body>
</html>