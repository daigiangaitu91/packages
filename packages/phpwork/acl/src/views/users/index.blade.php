
<!DOCTYPE html>
<html>
  <head>
    <title>Laravel Timezones</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >    
  </head>
  <body>
    <div class="container">      
      <div class="content">
        <a href="{{ route('users.create') }}" class="btn btn-primary">Create New User</a>

        <table class="table table-hover">
          <thead>
            <tr>
              <th>Name</th>
              <th>E-mail</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
            <tr>
              <td>
                <a href="{{ route('users.edit', $user->id) }}">{{ $user->name }}</a>
              </td>
              <td>{{ $user->email }}</td>
              <td>
                <a href="{{ route('users.edit', $user->id) }}">
                  <span class="glyphicon glyphicon-edit"></span>
                </a>
              </td>
              <td>
                <a href="{{ route('users.confirm', $user->id) }}">
                  <span class="glyphicon glyphicon-remove"></span>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </body>
</html>