
<!DOCTYPE html>
<html>
<head>
    <title>Laravel Timezones</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >
</head>
<body>
<div class="container">
    <div class="content">
        {!! Form::model($roles, [
        'method' => $roles->exists ? 'put' : 'post',
        'route' => $roles->exists ? ['roles.update', $roles->id] : ['roles.store']
        ]) !!}

        <div class="form-group">
            {!! Form::label('Title') !!}
            {!! Form::text('role_title', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('slug') !!}
            {!! Form::text('role_slug', null, ['class' => 'form-control']) !!}
        </div>

        </div>

        {!! Form::submit($roles->exists ? 'Save Role' : 'Create New Role', ['class' => 'btn btn-primary']) !!}

        {!! Form::close() !!}
    </div>
</div>
</body>
</html>