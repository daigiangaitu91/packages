<?php

namespace Phpwork\Acl;

use Illuminate\Support\ServiceProvider;

class TimezonesServiceProvider extends ServiceProvider {

  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot() {
    $this->loadViewsFrom(__DIR__.'/views', 'acl');
    // Publish database migrations
    $this->publishes([__DIR__ . '/database/migrations/' => base_path('/database/migrations')], 'migrations');
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register() {
    include __DIR__ . '/routes.php';
    $this->app->make('Phpwork\Acl\Controller\TimezonesController');
    $this->app->make('Phpwork\Acl\Controller\UserController');
    $this->app->make('Phpwork\Acl\Controller\RoleController');
    $this->app->make('Phpwork\Acl\Controller\PermissionController');
  }

}
