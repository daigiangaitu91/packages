<?php

Route::get('timezones/{timezone?}', 
  'phpwork\acl\Controller\TimezonesController@index');

//Route::get('user/create','phpwork\acl\Controller\UserController@create');

Route::resource('users', 'phpwork\acl\Controller\UserController', ['except' => ['show']]);
Route::get('users/{users}/confirm', ['as' => 'users.confirm', 'uses' => 'phpwork\acl\Controller\UserController@confirm']);

Route::resource('roles', 'phpwork\acl\Controller\RoleController', ['except' => ['show']]);
Route::get('roles/{roles}/confirm', ['as' => 'roles.confirm', 'uses' => 'phpwork\acl\Controller\RoleController@confirm']);

Route::resource('permissions', 'phpwork\acl\Controller\PermissionController', ['except' => ['show']]);
Route::get('permissions/{permissions}/confirm', ['as' => 'permissions.confirm', 'uses' => 'phpwork\acl\Controller\PermissionController@confirm']);