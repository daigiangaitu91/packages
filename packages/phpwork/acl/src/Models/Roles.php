<?php

namespace Phpwork\Acl\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = 'roles';

    protected $fillable = ['role_title', 'role_slug'];
    
//    public function users(){
//        return $this->belongsToMany('Phpwork\Acl\Models\User');
//    }

    public function permissions(){
        return $this->belongsToMany('Phpwork\Acl\Models\Permission');
    }

    public static function addNeededRoles(array $roles)
    {
        if (count($roles) === 0 ){
            return;
        }
        $found = static::whereIn('id',$roles)->lists('id')->all();

        foreach (array_diff($roles, $found) as $role) {

            static::create([
                'role' => $role,


            ]);
        }
    }
}
